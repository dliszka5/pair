#include <iostream>

using namespace std;

template<typename firstType, typename secondType> class TPair{
	firstType  first;
	secondType second;
public:
	TPair(firstType first = firstType(), secondType second = secondType()) : first(first), second(second) { }
	TPair(const TPair& obiekt){
		this->first = obiekt.first;
		this->second = obiekt.second;
	}

		const void setFirst(const firstType& first){
			this->first = first;
		}

		const void setSecond(const secondType& second){
			this->second = second;
		}

		const firstType getFirst() const {
			return this->first;
		}

		const secondType getSecond() const {
			return this->second;
		}

		const bool isEmpty() const {
			if(this->first){
				if(this->second){
					return false;
				}
			}

			return true;
		}

		void swap(TPair<firstType,secondType>& para){
			firstType& buf1 = para.first;
			secondType& buf2 = para.second;
				this->first = para.first;
				this->second = para.second;
				para.first = buf1;
				para.second = buf2;
		}


		TPair& make_pair(const firstType& first, const secondType& second){
			this->first = first;
			this->second = second;
				return *this;
		}

		const bool operator==(const TPair& obiekt) const {
			if(this->first == obiekt.first){
				if(this->second == obiekt.second){
					return true;
				}
			}
			return false;
		}

		const bool operator!=(const TPair& obiekt) const {
			if(this->first == obiekt.first){
				if(this->second == obiekt.second){
					return false;
				}
			}
			return true;
		}

		TPair& operator=(TPair& obiekt){
			this->first = obiekt.first;
			this->second = obiekt.second;

			return *this;
		}

		const TPair& operator=(const TPair& obiekt){
			this->first = obiekt.first;
			this->second = obiekt.second;
			return *this;
		}


		friend  ostream& operator << (ostream& out, TPair& obiekt){
			out << "first = " << obiekt.first << " second = " << obiekt.second;
			return out;
		}

		friend istream& operator >> (istream& in, TPair& obiekt){
			in >> obiekt.first >> obiekt.second;
				return in;
		}

		virtual ~TPair() { }


		/*
			isEmpty
		*/
	
};

template<typename type> class TComplex{
	type im;
	type re;
public:
	TComplex(type re = type(), type im = type()) : re(re),im(im) { }
	TComplex(const TComplex& obiekt){
		this->im = obiekt.im;
		this->re = obiekt.re;
	}
	void setRe(const type re){
		this->re = re;
	}

	void setIm(const type im){
		this->im = im;
	}

	const type getRe() const {
		return re;
	}

	const type getIm() const {
		return im;
	}

	const TComplex& operator=(const TComplex& obiekt){
		this->im = obiekt.im;
		this->re = obiekt.re;
			return *this;
	}

	const void operator+(const TComplex& obiekt){
		this->im += obiekt.im;
		this->re += obiekt.re;
	}

	const void operator-(const TComplex& obiekt){
		this->im -= obiekt.im;
		this->re -= obiekt.re;
	}

	const void operator+=(const TComplex& obiekt){
		this->im += obiekt.im;
		this->re += obiekt.re;
	}

	const void operator-=(const TComplex& obiekt){
		this->im -= obiekt.im;
		this->re -= obiekt.re;
	}

	const void operator+(){
		this->re = +this->re;
		this->im = +this->im;
	}

	const void operator-(){
		this->re = -this->re;
		this->im = -this->im;
	}

    const TComplex& operator*(const TComplex& comp){
        TComplex *temp = new TComplex;
        temp->re=(re*comp.re)+(im*comp.im);
        temp->im=(re*comp.im)+(im*comp.re);
        return *temp;
    }



	friend ostream& operator<<(ostream& out,const TComplex<type>& obiekt){
		out << "Re = " << obiekt.re << " Im = " << obiekt.im;
		return out;
	}

	friend istream& operator>>(istream& in,TComplex<type>& obiekt){
		in >> obiekt.re >> obiekt.im;
		return in;
	}

	virtual ~TComplex() { }	
};

int main(){

	TPair<int,double> obj(1, 2.3);

	TPair<int,double> obj2(6,-5);

	obj.swap(obj2);
	cout << obj;
	
	TComplex<double> com(2,-3);
	TPair<int,TComplex<double> > d;
	cout << d;
	d.make_pair(7,com);
	cout << d;

	return 0;
}